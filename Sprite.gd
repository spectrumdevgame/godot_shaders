tool
extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	_zoom_chnaged()

func _zoom_chnaged():
	var zoom=get_viewport().global_canvas_transform.y.y
	print(zoom)
	material.set_shader_param("y_zoom",zoom)


func _on_Sprite_item_rect_changed():
	material.set_shader_param("scale",Vector3(scale.x,scale.y,0.0))
