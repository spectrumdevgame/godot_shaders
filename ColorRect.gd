tool
extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_viewport_transform()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_zoom_chnaged()

func _zoom_chnaged():
	var zoom=get_viewport_transform().y.y
	#var zoom=get_viewport().global_canvas_transform.y.y
	material.set_shader_param("y_zoom",zoom)


func _on_ColorRect_item_rect_changed():
	material.set_shader_param("scale",Vector3(rect_size.x,rect_size.y,0.0))
